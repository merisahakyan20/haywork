﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Haywork.Core.Database
{
    public class UserWork
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int WorkId { get; set; }
        public decimal UserRate { get; set; }
        public decimal TotalEarned { get; set; }
        public User User { get; set; }
        public Work Work { get; set; }
    }
}
